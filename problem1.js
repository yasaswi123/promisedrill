const { error } = require('node:console')
const fs = require('node:fs').promises
const path = require('node:path')

function problem1(numberOfFiles) {
    const directory = './promiseFiles'
    fs.mkdir(directory)
        .then(() => {
            for (let index = 0; index < numberOfFiles; index++) {
                const fileName = `file${index + 1}.json`
                const filePath = path.join(directory, fileName)
                fs.writeFile(filePath, `file${index + 1} consists data`)
                console.log("file created successfully")
            }
        })
        .then(() => {
            for (let index = 0; index < numberOfFiles; index++) {
                const fileName = `file${index + 1}.json`
                const filePath = path.join(directory, fileName)
                fs.unlink(filePath)
                console.log("file deleted successfully")
            }
        })
        .catch((err) => console.error(err))
}

module.exports = problem1