const fs = require('node:fs').promises

function problem2() {
    fs.readFile('../lipsum.txt', 'utf-8')
        .then((data) => {
            return data.toUpperCase()
        })
        .then((upperText) => {
            fs.writeFile('./file1.txt', upperText)
            console.log("uppercase text file created successfully")
        })
        .then(() => {
            fs.appendFile('./filenames.txt', 'file1.txt\n')
        })
        .then(() => {
            return fs.readFile('./file1.txt', 'utf-8')
        })
        .then((data1) => {
            return data1.toLowerCase().split('.')
        })
        .then((lowerText) => {
            fs.writeFile('./file2.txt', JSON.stringify(lowerText))
            console.log("lowercase converted successfully")
        })
        .then(() => {
            fs.appendFile('./filenames.txt', 'file2.txt\n')
        })
        .then(() => {
            return fs.readFile('./file2.txt', 'utf-8')
        })
        .then((data2) => {
            return data2.split(',').sort()
        })
        .then((sorting) => {
            fs.writeFile('./file3.txt', sorting)
            console.log("sorted successfully")
        })
        .then(() => {
            fs.appendFile('./filenames.txt', 'file3.txt\n')
        })
        .then(() => {
            return fs.readFile('./filenames.txt', 'utf-8')
        })
        .then((data) => {
            data.split('\n').forEach((currentFile) => {
                if (currentFile != "") {
                    fs.unlink(currentFile)
                    console.log("file deleted successfully")
                }
            })
        })
        .catch((err) => console.log(err))
}

module.exports = problem2